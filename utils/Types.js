export const ARR = 'array';
export const OBJ = 'object';
export const STR = 'string';
export const INT = 'integer';
export const BOOL = 'boolean';
export const NULL = 'null';
