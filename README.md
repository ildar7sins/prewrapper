A Vue component to wrap preformatted text. It's check the element width and update it dynamically. 

## Get started

Install:

```bash
npm install pre-wrapper
```

Then, import and register the component:

```html
<template>
    <PreWrapper>{{ headers }}</PreWrapper>
</template>

<script>
import PreWrapper from './components/PreWrapper.vue'

export default {
    name: 'MyPage',
    components: {
        PreWrapper
    },
    data: () => ({
        host: 'example.com',
        'x-real-ip': '127.0.0.1',
        'content-length': '406',
        'user-agent': 'GuzzleHttp/7',
        'content-type': 'application/json',
        'Authorization': 'Bearer VnLnJ1L3NlcnZpY2VzL3NycyIsImV4cCI6MTeyJJ1L3NlcnZpY2VzL3NycyIsIMvcGF5IiwiYXVkIjoicm'
    })
}
</script>
```

## Preview
<img alt="preview" width="100%" src="https://gitlab.com/ildar7sins/prewrapper/-/raw/main/docs/preview.png"/>

## License

[MIT](https://github.com/sagalbot/vue-select/blob/master/LICENSE.md)
