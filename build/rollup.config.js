import vue from 'rollup-plugin-vue'; // Handle .vue SFC files
import buble from '@rollup/plugin-buble'; // Transpile/polyfill with reasonable browser support
import commonjs from '@rollup/plugin-commonjs'; // Convert CommonJS modules to ES6

export default {
    input: './src/index.js', // Path relative to package.json
    output: {
        name: 'PreWrapper',
        exports: 'named',
    },
    plugins: [
        vue(),
        commonjs(),
        buble(), // Transpile to ES5
    ],
};
